#include <iostream>
#include <tr1/functional>
#include <string>
#include <stdio.h>
#include <cstring>
#include <inttypes.h>
#include <assert.h>
#include <fstream>
#include <iomanip>      // std::setw

#include "pin.H"
#include "instlib.H"

//How to find the relative address of the basic block? To make it transperant to the 
//memory allocation policy of the OS

using namespace std;
using namespace INSTLIB;

FILTER filter;

UINT64 bblCount = 0;

string invalid = "invalid_rtn";

std::ofstream TraceFile;

/* ===================================================================== */
/* Commandline Switches */
/* ===================================================================== */
// Can use -o XXX.log to specifiy the output file
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "callstat.out", "specify trace file name");
// Can use -b to get the output based on bbl
KNOB<BOOL>   KnobBblTrace(KNOB_MODE_WRITEONCE, "pintool", "b", "0", "bbl trace ");
// Can use -m to get memory read write stat inside call/bbl
KNOB<BOOL>   KnobMemRW(KNOB_MODE_WRITEONCE, "pintool", "m", "0", "memory read write ");

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */
INT32 Usage()
{
    cerr << "This tool produces function call memory trace." << endl << endl;
    cerr << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

/* ===================================================================== */
// convert target address to function name
const string *Target2String(ADDRINT target)
{
    string routineName;
    PIN_LockClient();  // ensure RTN_find
    RTN rtn = RTN_FindByAddress(target);
    if (!RTN_Valid(rtn))
    {
        PIN_UnlockClient();
        return &invalid;
    }
    else
    {
        IMG img = SEC_Img(RTN_Sec(rtn));
        if (IMG_Valid(img))     
            routineName = IMG_Name(img) + ":" + RTN_Name(rtn);
        PIN_UnlockClient();
        return new string(routineName);
    }
}

/* ===================================================================== */
// print out the basic block routine name and image name
void do_bbl(ADDRINT target)
{
    const string *s = Target2String(target);
    TraceFile <<"BBL  " << setw(55)  << left << *s <<  "\tAddr  " << hex << target << endl;
}

/* ===================================================================== */
// print out the call routine name and image name
void do_call(ADDRINT target)
{
    const string *s = Target2String(target);
    TraceFile <<"Call  "  << setw(55) << left << *s << "\tAddr  " << hex << target << endl;
}


/* ===================================================================== */
// determine if the call is taken
VOID  do_call_indirect(ADDRINT target, BOOL taken)
{
    if( !taken ) return;
    do_call( target );
    
}

/* ===================================================================== */
// print out the call routine name and image name
void do_ret(CONTEXT * ctxt)
{
    ADDRINT AfterIP = (ADDRINT)PIN_GetContextReg( ctxt, REG_INST_PTR);
    const string *s = Target2String(AfterIP);
    TraceFile << "RET   " << *s  << endl;
}

/* ===================================================================== */
// Print a memory read record
VOID RecordMemRead(VOID * ip, VOID * addr)
{
    TraceFile << "MEM R  " << ip << "\t" << addr  << endl;
}

/* ===================================================================== */
// Print a memory write record
VOID RecordMemWrite(VOID * ip, VOID * addr)
{
    TraceFile << "MEM W  " << ip << "\t" << addr  << endl;
}


/* ===================================================================== */
void Trace(TRACE trace, void *v) 
{
    const BOOL USE_BBL = KnobBblTrace.Value();
    const BOOL MEM_RW = KnobMemRW.Value();

    if (!filter.SelectTrace(trace))
        return;

    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) 
    {
        if( USE_BBL)
        {
            BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)do_bbl, 
                                            IARG_ADDRINT, BBL_Address(bbl), 
                                            IARG_UINT32, BBL_NumIns(bbl), 
                                            IARG_END);
        }
        else
        {
            INS tail = BBL_InsTail(bbl);

            if( INS_IsCall(tail) )
            {
                if( INS_IsDirectBranchOrCall(tail) )
                {
                    const ADDRINT target = INS_DirectBranchOrCallTargetAddress(tail);
                    INS_InsertPredicatedCall(tail, IPOINT_BEFORE, AFUNPTR(do_call),
                                                 IARG_ADDRINT, target, IARG_END);
                }
                else
                {
               //     INS_InsertCall(tail, IPOINT_BEFORE, AFUNPTR(do_call_indirect),
               //                        IARG_BRANCH_TARGET_ADDR, IARG_BRANCH_TAKEN, IARG_END);
                    
                }
            }
            else if( INS_IsRet(tail) )
            {
                INS_InsertCall(tail, IPOINT_TAKEN_BRANCH, AFUNPTR(do_ret),
                                                IARG_CONTEXT, IARG_END);

            }
        }       
        // memory R/W
        if (MEM_RW)
        {
            for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
            {
            
                UINT32 memOperands = INS_MemoryOperandCount(ins);

                // Iterate over each memory operand of the instruction.
                for (UINT32 memOp = 0; memOp < memOperands; memOp++)
                {
                    if (INS_MemoryOperandIsRead(ins, memOp))
                    {
                        INS_InsertPredicatedCall(
                            ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,
                            IARG_INST_PTR,
                            IARG_MEMORYOP_EA, memOp,
                            IARG_END);
                    }
                    if (INS_MemoryOperandIsWritten(ins, memOp))
                    {
                        INS_InsertPredicatedCall(
                            ins, IPOINT_BEFORE, (AFUNPTR)RecordMemWrite,
                            IARG_INST_PTR,
                            IARG_MEMORYOP_EA, memOp,
                            IARG_END);
                    }
                }
            
            }
        }
            
    }
}

/* ===================================================================== */
void Fini(INT32 code, void *v) 
{
    TraceFile << "# eof" << endl;
    
    TraceFile.close();
}

/* ===================================================================== */
int main(int argc, char * argv[]) 
{
    
    PIN_Init(argc, argv);
    TraceFile.open(KnobOutputFile.Value().c_str());
    
    filter.Activate();

    TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddFiniFunction(Fini, 0);

    PIN_StartProgram();

    return 0;
}
