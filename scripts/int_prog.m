%{
Things to add:
1. Automatically block the current rows and find out the next.. check
%}

%% do int programming
%tic;
clear;
% set level of rows to be blocked
level = 1;

load gzip.mat
[m,n] = size(G);
intcon = 1:n;
A = -G;
b = -ones(1,m);
f = ones(1,n);
ub = ones(n,1);
lb = zeros(n,1);
% always block the 0 level rows, which are results of the synthetic malware
col_cov = 30:36;

% second level
% ub(37) = 0; ub(181) = 0; ub(246) = 0; ub(14952) = 0;
% third level
%ub(179:185) = zeros(1,7);

for i = 1:level 
    ub(col_cov) = 0; 
    x = intlinprog(f,intcon,A,b,[],[],lb,ub);
    col_cov = find(x);
end
%timelapsed = toc

%%
ylabel = cell(length(col_cov),1);
for i = 1: length(col_cov)
    ylabel(i) = cellstr( strcat('region',num2str(i)) );
end
tbl = array2table(-A(:,col_cov),'VariableNames', ylabel);

%tbl = table( -A(:,col_cov), 'RowNames', set_name (:,1));

%% find trace name
bbl_cnt =0; mem_cnt = 0;
for k = 1:size(col_cov,1)
    i = col_cov(k);
    index = find(G(:,i));
    %fprintf('col number: %d; with length %d\n', i, length(index));
    if iscellstr(bbl_region(i,1))
        %fprintf('bbl region: %s to %s\n', bbl_region{r,1},  bbl_region{r,2} );
        fprintf('bbl region with length %d: %d: (%s, %s),\n', length(index), i,  bbl_region{i,1},  bbl_region{i,2} );
        bbl_cnt = bbl_cnt + 1;
    else 
        %fprintf('mem region: %s to %s\n', mem_region{r,1},  mem_region{r,2} );
        fprintf('mem region with length %d: %d:(%s, %s),\n',  length(index),i, mem_region{i,1},  mem_region{i,2} );
        mem_cnt = mem_cnt + 1;
    end
end


%%
colormap(flipud(gray));
imagesc(-A(:,col_cov));
xlabel('lvl2 segments selected', 'FontSize', 22);
%title('Trace coverage using lvl2 memory segments', 'FontSize', 22);
set(gca,'TickLength',[0 0]);


%% 
index = find(G(:,181));
set_name(index,:)

%% 
cnt = 0;
for i = 1:752
    if strcmp(set_name(i,3), 'data')
        cnt = cnt +1;
    end
end