# README #
Dear Reader,
In this repository, we have provided the benchmark, source code, experiment traces for our HVC 2017 submission titled, "Trace-based Analysis of Memory Corruption Malware Attacks."

### Benchmark ###
The benchmark is instrumented version of RIPE to create both benign and malicious version of program runs.

### Pin-tool ###
Memory monitoring tool based on intel-pin.

### Memory traces ###
Memory traces collected and used for experiment.

### Data handling scripts ###
Python and matlab scripts used to help data handling.





